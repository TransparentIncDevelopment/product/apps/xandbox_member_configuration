import json
import requests
import yaml
import sys
import getopt

class Account:
    def __init__(self, bank_entity, name):
        self.bank_id = 1 if "one" in bank_entity["bank"] else 2 # Assumes test-bank-one is POSTed first
        self.name = name
        self.account_number = bank_entity["account"]["account_number"]

    def to_json(self):
        json_obj = {
            "bankId": int(self.bank_id),
            "shortName": str(self.name),
            "accountNumber": str(self.account_number)
        }
        return json_obj


class BankAdapter:
    def __init__(self, banks_entity, bank_entity, member_index, generated_location):
        self.adapter_type = "treasuryPrime" # Expected value by Member API!
        self.url = "http://bank-mocks:8888/" + bank_entity["bank"].replace("-", "_") # HARDCODED TREASURY PRIME INTERNAL BANK MOCKS URL Not available in /generated
        with open(generated_location + "/members/" + str(member_index) + "/member-api/config/secrets/secrets.yaml", 'r') as stream:
            try:
                secrets = yaml.safe_load(stream)
                bank_secrets = []
                for k, v in secrets.items():
                    match = bank_entity["bank"].lower().replace("-", "_")
                    if match in k:
                        bank_secrets.append(k)
                self.secretApiKeyId = str([i for i in bank_secrets if "username" in i][0])
                self.secretApiKeyValue = str([i for i in bank_secrets if "password" in i][0])
            except yaml.YAMLError as exc:
                print(exc)

    def to_json(self):
        json_obj = {
            self.adapter_type: {
                "url": str(self.url),
                "secretApiKeyId": str(self.secretApiKeyId),
                "secretApiKeyValue": str(self.secretApiKeyValue),
            }
        }
        return json_obj


class Bank:
    def __init__(self, bank_entity, member_index, banks_entity, trust_account, generated_location):
        self.name = bank_entity["bank"]
        self.routing_number = bank_entity["account"]["routing_number"]
        self.trust_account = trust_account
        self.adapter = BankAdapter(banks_entity, bank_entity, member_index, generated_location)
        self.account = Account(bank_entity, "Account 0")

    def to_json(self):
        json_obj = {
            "name": str(self.name),
            "routingNumber": str(self.routing_number),
            "claimsAccount": str(self.trust_account),
            "adapter": self.adapter.to_json(),
        }
        return json_obj


class Member:
    def __init__(self, member_entity, member_index, banks_entity, trust_account, generated_location):
        self.name = member_entity["entity-name"]
        self.base_api_url = member_entity["member-api-details"]["member-api-url"]
        try:
            self.api_token = member_entity["member-api-details"]["auth"]["token"]
        except:
            self.api_token = ""
        self.banks = [
            Bank(bank, member_index, banks_entity, trust_account, generated_location)
            for bank in member_entity["banks"]]



class Trust:
    def __init__(self, trust_entity):
        self.account = trust_entity["banks"][0]["account"]["account_number"]


def create_members(entity_members, banks_entity, trust_account, generated_location):
    return [
        Member(entity, idx, banks_entity, trust_account, generated_location)
        for idx, entity
        in enumerate(entity_members)
    ]

# Assumes the test_bank_one and test_bank_two base urls are their bank names
# Assumes single trust account throughout network
# Assumes single account per bank
# Assumes banks are named test-bank-one and test-bank-two
# Assumes banks are POSTing in order
# Assumes banks are using a TreasuryPrime adapter
def main(argv):
    generated_location = ''
    try:
        opts, args = getopt.getopt(argv,"g:")
    except getopt.GetoptError:
        sys.exit("option -g [generated_network_location] required.")
    for opt, arg in opts:
        if opt == '-g':
            generated_location = arg
    if generated_location == '':
        sys.exit("option -g [generated_network_location] required.")
    print("generated network is located at: " + generated_location)
    with open(generated_location + "/entities-metadata.yaml", 'r') as stream:
        try:
            entities = yaml.safe_load(stream)
            trust = Trust(entities["trust"])
            members = create_members(entities["members"], entities["banks"], trust.account, generated_location)

            failures = []
            successful_members_configured = []
            for member in members:
                headers = {
                    "accept": "application/json",
                    "Content-Type": "application/json",
                    "Authorization": "Bearer " + member.api_token
                }
                print("Configuring Member API for member: " + member.name)
                for bank in member.banks:
                    print("\tCreating " + bank.name)
                    r_b = requests.post(member.base_api_url+"api/v1/banks", json=bank.to_json(), headers=headers)
                    if r_b.status_code != 201:
                        failures.append("Bank: " + bank.name + " for member " + member.name)
                    else:
                        successful_members_configured.append(member.name)
                    print("\t\tReturn Status: " + str(r_b.status_code))
                    print("\tCreating Account " + bank.account.name)
                    r_a = requests.post(member.base_api_url+"api/v1/accounts", json=bank.account.to_json(), headers=headers)
                    if r_a.status_code != 201:
                        failures.append("Account (Bank " + bank.name + "): " + bank.account.name + " for member " + member.name)
                    else:
                        successful_members_configured.append(member.name)
                    print("\t\tReturn Status: " + str(r_a.status_code))
            print("Finished configuring banks and accounts.\n")
            print("Successfully Configured: \n\t" + "\n\t".join(list(set(successful_members_configured))) + "\n")
            print("Failures: \n\t " + "\n\t ".join(failures)+"\n")
            if len(failures) > 0:
                sys.exit("Failures during configuration POSTs")
            sys.exit()
        except yaml.YAMLError as exc:
            sys.exit(exc)


if __name__ == '__main__':
    main(sys.argv[1:])
